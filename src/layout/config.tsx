import { CrownFilled, TabletFilled, UserOutlined } from '@ant-design/icons';

// eslint-disable-next-line import/no-anonymous-default-export
export default {
    menu: {
        collapsedShowGroupTitle: false,
    },
    bgLayoutImgList: [
        {
            src: '/bg_3.png',
            left: 85,
            bottom: 100,
            height: '303px',
        },
        {
            src: '/bg_2.png',
            bottom: -68,
            right: -45,
            height: '303px',
        },
        {
            src: '/bg_1.png',
            bottom: 0,
            left: 0,
            width: '331px',
        },
    ],
    route: {
        path: '/',
        routes: [
            {
                name: 'Home',
                icon: <TabletFilled />,
                path: '/',
                routes: [
                    {
                        path: 'table',
                        name: 'List',
                        indexRoute: {
                            component: '/table',
                        },
                        icon: <CrownFilled />,
                    },
                ],
            },
        ],
    },
    appList: [
        {
            icon: 'https://www.logo.wine/a/logo/Facebook/Facebook-Logo.wine.svg',
            title: 'Diệu chiêm',
            desc: 'chiempt',
            url: 'https://www.facebook.com/chiem.dieu.18/',
            target: '_blank',
        },
        {
            icon: 'https://www.logo.wine/a/logo/Facebook/Facebook-Logo.wine.svg',
            title: 'Diệu chiêm',
            desc: 'chiempt',
            url: 'https://www.facebook.com/chiem.dieu.18/',
            target: '_blank',
        },
        {
            icon: 'https://www.logo.wine/a/logo/Facebook/Facebook-Logo.wine.svg',
            title: 'Diệu chiêm',
            desc: 'chiempt',
            url: 'https://www.facebook.com/chiem.dieu.18/',
            target: '_blank',
        },
        {
            icon: 'https://www.logo.wine/a/logo/Facebook/Facebook-Logo.wine.svg',
            title: 'Diệu chiêm',
            desc: 'chiempt',
            url: 'https://www.facebook.com/chiem.dieu.18/',
            target: '_blank',
        },
        {
            icon: 'https://www.logo.wine/a/logo/Facebook/Facebook-Logo.wine.svg',
            title: 'Diệu chiêm',
            desc: 'chiempt',
            url: 'https://www.facebook.com/chiem.dieu.18/',
            target: '_blank',
        },

    ],
};