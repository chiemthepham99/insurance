import {
    LogoutOutlined,
    GithubFilled,
    PlusCircleFilled,
    SearchOutlined,
    ArrowLeftOutlined
} from '@ant-design/icons';
import ProCard from '@ant-design/pro-card';
import type { ProSettings } from '@ant-design/pro-layout';
import { PageContainer, ProLayout, SettingDrawer } from '@ant-design/pro-layout';
import { Button, ConfigProvider, Divider, Input, theme } from 'antd';
import React, { useState } from 'react';
import defaultProps from './config';
import { Outlet, Link, useLocation, useNavigate } from "react-router-dom";
import enUS from 'antd/lib/locale/en_US';

const MenuCard = () => {
    return (
        <div
            style={{
                display: 'flex',
                alignItems: 'center',
            }}
        >
            <Divider
                style={{
                    height: '1.5em',
                }}
                type="vertical"
            />
        </div>
    );
};

const SearchInput = () => {
    const { token } = theme.useToken();
    return (
        <div
            key="SearchOutlined"
            aria-hidden
            style={{
                display: 'flex',
                alignItems: 'center',
                marginInlineEnd: 24,
            }}
            onMouseDown={(e) => {
                e.stopPropagation();
                e.preventDefault();
            }}
        >
            <Input
                style={{
                    borderRadius: 4,
                    marginInlineEnd: 12,
                    backgroundColor: token.colorBgTextHover,
                }}
                prefix={
                    <SearchOutlined
                        style={{
                            color: token.colorTextLabel,
                        }}
                    />
                }
                placeholder="Tìm tiếm"
                bordered={false}
            />
            <PlusCircleFilled
                style={{
                    color: token.colorPrimary,
                    fontSize: 24,
                }}
            />
        </div>
    );
};

const Layout: React.FC = () => {
    const location = useLocation()
    const [settings, setSetting] = useState<Partial<ProSettings> | undefined>({
        fixSiderbar: true,
        splitMenus: false,
        colorPrimary: '#205737',
        layout: 'mix',
    });
    let navigate = useNavigate();

    const [pathname, setPathname] = useState(location.pathname);
    const [num, setNum] = useState(40);
    return (
        <div
            style={{
                height: '100vh',
            }}
        >
            <ConfigProvider locale={enUS}>
                <ProLayout
                    {...defaultProps}
                    location={{
                        'pathname': pathname,
                    }}
                    siderMenuType="sub"
                    avatarProps={{
                        src: 'https://scontent.fhan2-4.fna.fbcdn.net/v/t39.30808-6/294506157_726691405295480_3402026236590746238_n.jpg?stp=cp6_dst-jpg&_nc_cat=105&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=LtA5VSjJwU4AX8tyOTs&_nc_ht=scontent.fhan2-4.fna&oh=00_AfAGi2OcM5zTmA1nq7TvWJuLrVy-StTsnhbDdvL51k6S3Q&oe=63AFD10C',
                        size: 'small',
                        title: 'Chiempt',
                    }}
                    actionsRender={(props) => {
                        if (props.isMobile) return [];
                        return [
                            props.layout !== 'side' && document.body.clientWidth > 1400 ? (
                                <SearchInput />
                            ) : undefined,
                            <GithubFilled key="GithubFilled" />,
                        ];
                    }}
                    headerTitleRender={(logo, title, _) => {
                        const defaultDom = (
                            <a href={'/'}>
                                <img src='/logo.png'
                                    style={{
                                        width: '100%',
                                        height: 55,
                                        marginRight: 4
                                    }}
                                    className='w-full'></img>
                                <span className='uppercase font-medium' style={{color: '#438c4d'}}> {"INSURANCE"}</span>
                            </a>
                        );
                        if (document.body.clientWidth < 1400) {
                            return (<img src='/logo.png'></img>);
                        }
                        if (_.isMobile) return defaultDom;
                        return (
                            <>
                                {defaultDom}
                                <MenuCard />
                            </>
                        );
                    }}
                    menuFooterRender={(props) => {
                        if (props?.collapsed) return <Button icon={<LogoutOutlined />}></Button>;
                        return (
                            <div
                                style={{
                                    textAlign: 'center',
                                    paddingBlockStart: 12,
                                }}
                            >
                                <Button icon={<LogoutOutlined />} onClick={(event) => navigate('/auth/login')}>Logout</Button>
                            </div>
                        );
                    }}
                    onMenuHeaderClick={(e) => console.log(e)}
                    menuItemRender={(item, dom) => (
                        <Link
                            to={item.path || '/'}
                            onClick={() => {
                                const path = item.path
                                setPathname(path || '/welcome');

                                console.log(pathname, 'pathname')
                            }}
                        >
                            {dom}
                        </Link>
                    )}
                    {...settings}
                >
                    <PageContainer
                        token={{
                            paddingInlinePageContainerContent: num,
                        }}
                        extra={[
                            <Button
                                key="1"
                                type="primary"
                                icon={<ArrowLeftOutlined />}
                                onClick={() => {
                                    setNum(num > 0 ? 0 : 40);
                                }}
                            >
                                Back
                            </Button>,
                        ]}
                    >
                        <ProCard
                            style={{
                                height: '200vh',
                                minHeight: 800,
                            }}
                        >
                            <Outlet />
                        </ProCard>
                    </PageContainer>

                    <SettingDrawer
                        pathname={pathname}
                        enableDarkTheme
                        getContainer={() => document.getElementById('test-pro-layout')}
                        settings={settings}
                        onSettingChange={(changeSetting) => {
                            setSetting(changeSetting);
                        }}
                        disableUrlParams={false}
                    />
                </ProLayout>
            </ConfigProvider>
        </div>
    );
};
export default Layout;