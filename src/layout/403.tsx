import React from 'react';
import {Button, Result} from 'antd';
import {Route} from "react-router";

const App: React.FC = () => (
    <Result
        status="404"
        title="404"
        subTitle="Sorry, the page you visited does not exist."
        extra={<Button type="primary">
            <Route path={'/'}>
                Back Home
            </Route>
        </Button>}
    />
);

export default App;