import React from "react";
import { Outlet } from "react-router-dom";
const BlankLayout: React.FC = () => {
  return (
    <>
      <div
        style={{
          height: "100%",
        }}
      >
        <Outlet />
      </div>
    </>
  );
};
export default BlankLayout;
