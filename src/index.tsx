import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";
import ErrorPage from "./layout/403"

import reportWebVitals from './reportWebVitals';
import Category from "./pages/category";
import Login from './pages/auth/Login';
import BlankLayout from './layout/Blank';
import store from './store'
import { Provider } from 'react-redux'
import { Spin } from 'antd';
import Spinning from './components/Spin'
Spin.setDefaultIndicator(<Spinning/>)
const router = createBrowserRouter([
    {
        path: "/",
        element: <App />,
        errorElement: <ErrorPage />,
        children: [
            {
                path: "/table",
                element: <Category />,
            },
        ]
    },
    {
        path: "/auth",
        element: <BlankLayout />,
        children: [
            {
                path: "/auth/login",
                element: <Login />
            }
        ]
    }

]);

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <Provider store={store}>
        <RouterProvider router={router} />
    </Provider>
);

reportWebVitals();
