import axios from "axios";

const request = axios.create({
    baseURL: 'http://localhost:8080',
    timeout: 3000,
    transformRequest: transformData
})

function transformData (data: any) {
    return data
}
export default request;