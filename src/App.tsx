import React from 'react';
import './App.css';
import Layout from "./layout/Layout";

function App() {
    return (
        <div className={'AppName'}>
            <Layout />
        </div>
    );
}

export default App;
