
import { Row, Input, Col, Form, Checkbox, Button, notification } from "antd";
import { useState } from "react";
import { useNavigate } from "react-router-dom"
import { useSelector, useDispatch } from 'react-redux'
import { increment } from '../../store/counterSlice'
interface FormType {
  username?: string
  password?: string,
  remember?: boolean
}
const Login: React.FC = () => {
  const count = useSelector((state: any) => state.counter.value)
  const dispatch = useDispatch()
  dispatch(increment)

  let navigate = useNavigate();
  console.log(count, navigate)
  const [state] = useState<FormType>({
    username: '',
    password: '',
    remember: false
  })

  const onFinish = (values: any) => {
    console.log(values)
    console.log(state)
    dispatch(increment)
    console.log(count)
    notification.success({
      message: 'Login Succes',
      description: `Hello ${state.username}`
    })
    navigate('/');
  }
  const onFinishFailed = () => {

  }


  return (
    <div
      className="overflow-hidden"
      style={{
        backgroundColor: "white",
        height: "100vh",
      }}
    >
      <Row
        className="overflow-hidden"
        style={{
          minHeight: '100vh',
          padding: 0
        }}>
        <Col span={12}>
          <div className="bg-slate-200 h-full min-h-full">
            <img className="w-full h-full" src="https://github.githubassets.com/images/modules/logos_page/Octocat.png" alt="" />
          </div>
        </Col>
        <Col span={12}>
          <div className="flex justify-center items-center h-full w-full min-w-full">
            <Form
              name="Login"
              labelCol={{ span: 24 }}
              wrapperCol={{ span: 24 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="on"
            >
              <Form.Item
                label="Username"
                name="username"
                rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input value={state.username} onChange={(event) => state.username = event.target.value} />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: 'Please input your password!' }]}
              >
                <Input.Password value={state.password}  onChange={(event) => state.password = event.target.value} />
              </Form.Item>

              <Form.Item name="remember" valuePropName="checked">
                <Checkbox value={state.remember} onChange={(event) => state.remember = event.target.value} >Remember me</Checkbox>
              </Form.Item>

              <Form.Item className="flex justify-center mt-4">
                <Button type="primary" htmlType="submit">
                  Login
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Col>
      </Row>
    </div >
  );
};

export default Login;
