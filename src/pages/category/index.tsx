import React, { useState } from "react";
import { ProTable } from '@ant-design/pro-components';
import { Button, Modal, Form, Input, Select, Row, Col } from "antd"
import { PlusOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons"
import type { ProColumns } from '@ant-design/pro-components';
import axios from "axios";

const { Option } = Select;
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
};

const Category: React.FC = () => {
    const [dataSource, setDataSource] = useState<Array<any>>([])

    const [loading, setLoading] = useState<boolean>(false)

    const [isModalOpen, setIsModalOpen] = useState<boolean>(false)

    const columns: ProColumns<any>[] = [
        {
            title: 'ID',
            dataIndex: 'id',
            ellipsis: true,
            copyable: true,
            align: 'center',
            tooltip: 'ID',
            width: 150
        },
        {
            title: 'Name',
            ellipsis: true,
            dataIndex: 'name',
            width: 180
        },
        {
            title: 'Type',
            dataIndex: 'type',
            width: 180
        },
        {
            title: 'Icon',
            dataIndex: 'icon',
            copyable: true,
            width: 180
        },
        {
            title: 'Description',
            dataIndex: 'description',
        },
        {
            title: 'Action',
            dataIndex: 'option',
            valueType: 'option',
            align: 'center',
            width: 250,
            render: (_, record: any) => [
                <Button
                    type="primary"
                    style={{
                        alignItems: 'center',
                    }}
                    icon={
                        <EditOutlined
                            style={{
                                marginRight: '8px',
                            }}
                        />
                    }
                >
                    Edit
                </Button>,
                <Button
                    danger
                    type="primary"
                    style={{
                        alignItems: 'center',
                    }}
                    onClick={(): void => {
                        onDel(record)
                    }}
                    icon={
                        <DeleteOutlined
                            style={{
                                marginRight: '8px',
                            }}
                        />
                    }
                >
                    Delete
                </Button>,
            ],
        },
    ]

    const getData = async () => {
        try {
            setLoading(true)
            const data = await axios.get('http://localhost:8080/api/v1/categories/findAll')
            if (data && data.data) {
                setDataSource(data.data)
            }
            return data.data
        } finally {
            setLoading(false)
        }
    }

    const onDel = async (record: any): Promise<void> => {
        await axios.delete(`http://localhost:8080/api/v1/categories/${record.id}`).then(r => console.log(r))
        await getData()
    }

    const handleOk = () => {
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const onOpenModal = () => {
        setIsModalOpen(true)
    }

    return (
        <>
            <Modal title="Create Category" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <Form {...layout} name="control-ref">

                </Form>
            </Modal>
            <Row>
                <Col span={24}>
                    <ProTable
                        loading={loading}
                        request={getData}
                        dataSource={dataSource}
                        columns={columns}
                        cardBordered={true}
                        options={{
                            search: true,
                        }}
                        tableLayout='auto'
                        search={{
                            defaultCollapsed: true,
                            optionRender: (searchConfig, formProps, dom) => [
                                <Button
                                    key="reset"
                                >
                                    Reset
                                </Button>,
                            ],
                        }}
                        toolBarRender={() => [
                            <Button
                                type="primary"
                                key="primary"
                                onClick={onOpenModal}
                            >
                                <PlusOutlined />
                                Create
                            </Button>,
                        ]}
                        headerTitle={'List Category'}
                    >
                    </ProTable>
                </Col>
            </Row>
        </>
    )
}

export default Category;